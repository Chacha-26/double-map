const stores = new WeakMap();
function getStore<K1, K2, V>(dm: DoubleMap<K1, K2, V>): Map<K1, Map<K2, V>> {
    const store: Map<K1, Map<K2, V>> = stores.get(dm);
    if (store) {
        return store;
    } else {
        throw new TypeError("Method of DoubleMap called on incompatible receiver");
    }
}

export default class DoubleMap<K1, K2, V> {

    constructor(iterable?: Iterable<[K1, K2, V]>) {
        stores.set(this, new Map());
        
        if (iterable) {
            for (const [key1, key2, val] of iterable) {
                this.set(key1, key2, val);
            }
        }
    }

    get size(): number {
        let size = 0;
        for (const inner of getStore(this).values()) {
            size += inner.size;
        }
        return size;
    }

    get(key1: K1, key2: K2): V | undefined {
        const inner = getStore(this).get(key1);
        return inner && inner.get(key2);
    }

    set(key1: K1, key2: K2, val: V): this {
        const inner = getStore(this).get(key1);
        if (inner) {
            inner.set(key2, val);
        } else {
            getStore(this).set(key1, new Map<K2, V>().set(key2, val));
        }
        return this;
    }

    has(key1: K1, key2: K2): boolean {
        const inner = getStore(this).get(key1);
        return inner ? inner.has(key2): false;
    }

    delete(key1: K1, key2: K2): boolean {
        const inner = getStore(this).get(key1);
        if (inner && inner.has(key2)) {
            if (inner.size > 1) {
                inner.delete(key2);
            } else {
                getStore(this).delete(key1);
            }
            return true;
        }
        return false;
    }

    clear(): void {
        getStore(this).clear();
    }

    forEach(callback: (k1: K1, k2: K2, v: V, self: this) => void, thisArg?: Object): void {
        if (thisArg === undefined) {
            for (const [key1, inner] of getStore(this).entries()) {
                for (const [key2, val] of inner.entries()) {
                    callback(key1, key2, val, this);
                }
            }
        } else {
            for (const [key1, inner] of getStore(this).entries()) {
                for (const [key2, val] of inner.entries()) {
                    callback.call(thisArg, key1, key2, val, this);
                }
            }
        }
    }

    * entries(): IterableIterator<[K1, K2, V]> {
        for (const [key1, inner] of getStore(this)) {
            for (const [key2, val] of inner) {
                yield [key1, key2, val];
            }
        }
    }

    * keys(): IterableIterator<[K1, K2]> {
        for (const [key1, inner] of getStore(this)) {
            for (const key2 of inner.keys()) {
                yield [key1, key2];
            }
        }
    }

    * values(): IterableIterator<V> {
        for (const inner of getStore(this).values()) {
            yield * inner.values();
        }
    }

    * [Symbol.iterator](): IterableIterator<[K1, K2, V]> {
        for (const [key1, inner] of getStore(this)) {
            for (const [key2, val] of inner) {
                yield [key1, key2, val];
            }
        }
    }
}
