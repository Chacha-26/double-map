export default class DoubleMap<K1, K2, V> {
    constructor(iterable?: Iterable<[K1, K2, V]>);
    readonly size: number;
    get(key1: K1, key2: K2): V | undefined;
    set(key1: K1, key2: K2, val: V): this;
    has(key1: K1, key2: K2): boolean;
    delete(key1: K1, key2: K2): boolean;
    clear(): void;
    forEach(callback: (key1: K1, key2: K2, val: V, self: this) => void, thisArg?: Object): void;
    entries(): IterableIterator<[K1, K2, V]>;
    keys(): IterableIterator<[K1, K2]>;
    values(): IterableIterator<V>;
    [Symbol.iterator](): IterableIterator<[K1, K2, V]>;
}
