# Double-Map  
## Description  
An implementation of a map with two keys.  
Usage is identical to ES6 maps but with two keys.  
This will become fairly useless if value-types ever become a thing.  
See index.d.ts for a list of all methods and their signatures.  
## Usage
### JavaScript
```js
    var DoubleMap = require('double-map').default;
    // The constructor takes an optional iterator of 3-tuples.
    var dm = new DoubleMap([["k1", "k2", "v1"], /* ... */])
    // Both keys are necessary to access the value
    dm.get("k1", "k2") // "v1"
    // Keys are ordered
    dm.has("k2", "k1") // false
```
### TypeScript
```ts
   import DoubleMap from 'double-map';
   let dm = new DoubleMap<number, number, string>();
   // Duplicate values will be overridden
   dm.set(1, 2, "3").set(1, 2, "4");
   // The .keys() iterator yields the keys as a 2-tuple
   dm.keys().next().value; // [1, 2] as [number, number]
   dm.values().next().value; // "4" as string
   dm.entries().next().value; // [1, 2, "4"] as [number, number, string]
   // DoubleMaps are iterable
   for (const [k1, k2, v] of dm) { }
```