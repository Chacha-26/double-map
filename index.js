"use strict";
const stores = new WeakMap();
function getStore(dm) {
    const store = stores.get(dm);
    if (store) {
        return store;
    }
    else {
        throw new TypeError("Method of DoubleMap called on incompatible receiver");
    }
}
class DoubleMap {
    constructor(iterable) {
        stores.set(this, new Map());
        if (iterable) {
            for (const [key1, key2, val] of iterable) {
                this.set(key1, key2, val);
            }
        }
    }
    get size() {
        let size = 0;
        for (const inner of getStore(this).values()) {
            size += inner.size;
        }
        return size;
    }
    get(key1, key2) {
        const inner = getStore(this).get(key1);
        return inner && inner.get(key2);
    }
    set(key1, key2, val) {
        const inner = getStore(this).get(key1);
        if (inner) {
            inner.set(key2, val);
        }
        else {
            getStore(this).set(key1, new Map().set(key2, val));
        }
        return this;
    }
    has(key1, key2) {
        const inner = getStore(this).get(key1);
        return inner ? inner.has(key2) : false;
    }
    delete(key1, key2) {
        const inner = getStore(this).get(key1);
        if (inner && inner.has(key2)) {
            if (inner.size > 1) {
                inner.delete(key2);
            }
            else {
                getStore(this).delete(key1);
            }
            return true;
        }
        return false;
    }
    clear() {
        getStore(this).clear();
    }
    forEach(callback, thisArg) {
        if (thisArg === undefined) {
            for (const [key1, inner] of getStore(this).entries()) {
                for (const [key2, val] of inner.entries()) {
                    callback(key1, key2, val, this);
                }
            }
        }
        else {
            for (const [key1, inner] of getStore(this).entries()) {
                for (const [key2, val] of inner.entries()) {
                    callback.call(thisArg, key1, key2, val, this);
                }
            }
        }
    }
    *entries() {
        for (const [key1, inner] of getStore(this)) {
            for (const [key2, val] of inner) {
                yield [key1, key2, val];
            }
        }
    }
    *keys() {
        for (const [key1, inner] of getStore(this)) {
            for (const key2 of inner.keys()) {
                yield [key1, key2];
            }
        }
    }
    *values() {
        for (const inner of getStore(this).values()) {
            yield* inner.values();
        }
    }
    *[Symbol.iterator]() {
        for (const [key1, inner] of getStore(this)) {
            for (const [key2, val] of inner) {
                yield [key1, key2, val];
            }
        }
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = DoubleMap;
