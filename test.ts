import DoubleMap from '.';
import test from 'ava';

test("constructor", (t) => {
    t.notThrows(() => new DoubleMap());
    t.notThrows(() => new DoubleMap([[1,"foo",3], [undefined,{},6]] as Iterable<[{},{},{}]>));
    t.true(new DoubleMap() instanceof DoubleMap);
});

test("set/get/has/delete", (t) => {
    const x = new DoubleMap<number, number, number>();
    t.is(x.set(1,2,3), x);
    t.is(x.get(1,2), 3);
    t.true(x.has(1,2));

    t.is(x.get(2,1), undefined);
    t.false(x.has(2,1));

    t.is(x.set(1,2,4), x);
    t.is(x.get(1,2), 4);
    t.true(x.has(1,2));

    t.true(x.delete(1,2));
    t.false(x.has(1,2));
    t.is(x.get(1,2), undefined);
    t.false(x.delete(1,2));

    const y =  new DoubleMap<{},{},{}>();
    const o1 = {}, o2 = {};
    y.set(o1, o1, o1).set(o1, o2, o2);
    t.true(y.has(o1, o1));
    t.true(y.has(o1, o2));
    t.false(y.has(o2, o1));
    t.false(y.has(o2, o2));
    t.true(y.delete(o1, o1));
    t.false(y.has(o1, o1));
    t.true(y.has(o1, o2));
    t.false(y.delete(o1, o1));
});

test("clear/size", (t) => {
    let x = new DoubleMap();
    t.is(x.size, 0);
    x.set(1,2,3).set(4,5,6).set(7,8,9);
    t.is(x.size, 3);
    x.delete(4,5);
    t.is(x.size, 2);
    x.set(1,2,5).set(1,3,2).set(1,1,1);
    t.is(x.size, 4);
    t.is(x.clear(), undefined);
    t.is(x.size, 0);
});

test("iteration", (t) => {
    const arr = [[1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], [3,2,1]].map(Object.freeze) as [number, number, number][];
    let x = new DoubleMap<number, number, number>(arr), n: number;

    // .entries()
    n = 0;
    for (let v of x.entries()) {
        t.deepEqual(v, arr[n++]);
        t.false(Object.isFrozen(v));
    }
    t.is(n, arr.length);

    // .keys()
    n = 0;
    for (let v of x.keys()) {
        t.deepEqual(v, arr[n++].slice(0,2));
        t.false(Object.isFrozen(v));
    }
    t.is(n, arr.length);

    // .values()
    n = 0;
    for (let v of x.values()) {
        t.is(v, arr[n++][2]);
    }
    t.is(n, arr.length);

    // [Symbol.iterator]()
    n = 0;
    for (let v of x) {
        t.deepEqual(v, arr[n++]);
        t.false(Object.isFrozen(v));
    }
    t.is(n, arr.length);
});

test("forEach", (t) => {
    const arr = [[1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], [3,2,1]].map(Object.freeze) as [number, number, number][];
    let x = new DoubleMap<number,number,number>(arr), n: number;

    // without call
    n = 0;
    x.forEach((k1, k2, v, s) => {
        const cmp = arr[n++];
        t.is(k1, cmp[0]);
        t.is(k2, cmp[1]);
        t.is(v, cmp[2]);
        t.is(s, x);
    });
    t.is(n, arr.length);

    // with call
    n = 0;
    let obj = {};
    x.forEach(function (this, k1, k2, v, s) {
        const cmp = arr[n++];
        t.is(this, obj);
        t.is(k1, cmp[0]);
        t.is(k2, cmp[1]);
        t.is(v, cmp[2]);
        t.is(s, x);
    }, obj);
    t.is(n, arr.length);
});

test("Invalid call", (t) => {
    t.throws(() => DoubleMap.prototype.clear(), "Method of DoubleMap called on incompatible receiver");
});